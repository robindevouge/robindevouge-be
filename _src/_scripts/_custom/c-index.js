import Rnd from '../_modules/random.js';
import FpsCtrl from '../_modules/fpsCtrl.js';
import sScroll from '../_modules/smoothScroll.js';

var canvas = document.querySelector('.starsBg'),
	ww = window.innerWidth,
	wh = window.innerHeight;


////////// STARS //////////

const starAmount = 150;
var stars = [];

var Star = function() {
	this.radius = Rnd.between(1, 3);
	this.x = Rnd.between(0, ww);
	this.y = Rnd.between(0, wh);
	this.opacity = Rnd.between(3, 8) / 10;
	this.speedX = Rnd.decimal(0.04, 0.14);
};

Star.prototype.draw = function() {
	ctx.beginPath();
	ctx.fillStyle = 'rgba(255,255,255,' + this.opacity + ')';
	ctx.arc(this.x, this.y, this.radius / 2, 0, Math.PI * 2);
	ctx.fill();
	// ctx.drawImage(starCanvas, this.x, this.y, this.radius, this.radius);
}

Star.prototype.update = function() {
	this.x += this.speedX;

	if(this.x < -this.radius || this.x > ww + this.radius) {
		this.x = 0 - this.radius;
	}
}

for(var i = 0; i < starAmount; i++) {
	stars.push(new Star());
}


////////// CANVAS //////////

canvas.width = ww;
canvas.height = wh;
var ctx = canvas.getContext('2d');


////////// RENDER //////////

var render = new FpsCtrl(30, function(e) {

	// stars

	ctx.clearRect(0, 0, ww, wh);

	for(var i = 0; i < stars.length; i++) {
		stars[i].draw();
		stars[i].update();
	}
});

render.start();


////////// CTA CLICK //////////

var cta = document.querySelector('.header__cta');
cta.addEventListener('click', function(event) {
	event.preventDefault();
	sScroll.scrollTo(wh, 700);
})


////////// PROJECTS TOUCH //////////

var tiles = document.querySelectorAll('.tile');

for(var i = 0; i < tiles.length; i++) {
	tiles[i].children[0].addEventListener('click', function() {
		var parent = this.parentNode;
		if(parent.classList.contains('tile--open')) {
			parent.classList.remove('tile--open');
		} else {
			parent.classList.add('tile--open');
		}
	})
}

////////// TILE FOCUS FIX //////////

var tilesButtons = document.querySelectorAll('.tile__button');
var tilesLinks = document.querySelectorAll('.tile .link');

for(var i = 0; i < tilesButtons.length; i++) {
	tilesButtons[i].addEventListener('focusin', function() {
		this.parentNode.parentNode.classList.add('tile--hovered');
	})
	tilesButtons[i].addEventListener('focusout', function() {
		this.parentNode.parentNode.classList.remove('tile--hovered');
	})
}
for(var i = 0; i < tilesLinks.length; i++) {
	tilesLinks[i].addEventListener('focusin', function() {
		this.parentNode.parentNode.parentNode.classList.add('tile--hovered');
	})
	tilesLinks[i].addEventListener('focusout', function() {
		this.parentNode.parentNode.parentNode.classList.remove('tile--hovered');
	})
}

////////// ROCKET PARTICLES //////////

var emitter = document.querySelector('.particleEmitter');

function initparticles() {
	var firecount = 30;
	for(var i = 0; i <= firecount; i++) {
		var size = Rnd.between(8, 12);
		var particle = document.createElement("span");
		particle.classList.add("particle");
		particle.style.top = Rnd.between(70, 90) + "%";
		particle.style.left = Rnd.between(0, 80) + "%";
		particle.style.width = size + "px";
		particle.style.height = size + "px";
		particle.style.animationDelay = Rnd.between(0, 20) / 10 + "s";
		emitter.appendChild(particle);
	}
}

if(ww >= 1000) initparticles();
