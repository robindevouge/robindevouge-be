var Random = function() {};

Random.prototype.decimal = function(m, n) {
	return parseFloat((Math.random() * (n - m) + m).toFixed(3));
}

Random.prototype.between = function(m, n) {
	m = parseInt(m);
	n = parseInt(n);
	return Math.floor(Math.random() * (n - m + 1)) + m;
}

var Rnd = new Random;

export default Rnd;
