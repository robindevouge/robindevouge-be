// Adapted from Forestrf
// http://jsfiddle.net/forestrf/tPQSv/2/

var SmoothScroll = function (){}

SmoothScroll.prototype.scrollTo = function (element, duration) {
  var e = document.documentElement;
  if(e.scrollTop===0){
    var t = e.scrollTop;
    ++e.scrollTop;
    e = t+1===e.scrollTop--?e:document.body;
  }
  this.scrollToC(e, e.scrollTop, element, duration);
}

SmoothScroll.prototype.scrollToC = function (element, from, to, duration) {
  if (duration < 0) return;
  this.scrollToX(element, from, to, 0, 1/duration, 20, this.easeOutCuaic);
}

SmoothScroll.prototype.scrollToX = function (element, x1, x2, t, v, step, operacion) {
  if (t < 0 || t > 1 || v <= 0) return;
  element.scrollTop = x1 - (x1-x2)*operacion(t);
  t += v * step;
  setTimeout((function() {
    this.scrollToX(element, x1, x2, t, v, step, operacion);
}).bind(this), step);
}

SmoothScroll.prototype.easeOutCuaic = function (t){
  t--;
  return t*t*t+1;
}

var sScroll = new SmoothScroll;

export default sScroll;
