import './_custom/hidpi-canvas.js';

import Rnd from './_modules/random.js';
import FpsCtrl from './_modules/fpsCtrl.js';

var canvas = document.querySelector('.starsBg'),
	ww = window.innerWidth,
	wh = window.innerHeight;


////////// STARS //////////

const starAmount = 150;
var stars = [];

var Star = function() {
	this.radius = Rnd.between(1, 3);
	this.x = Rnd.between(0, ww);
	this.y = Rnd.between(0, wh);
	this.opacity = Rnd.between(3, 8) / 10;
	this.speedX = Rnd.decimal(0.04, 0.14);
};

Star.prototype.draw = function() {
	ctx.beginPath();
	ctx.fillStyle = 'rgba(255,255,255,' + this.opacity + ')';
	ctx.arc(this.x, this.y, this.radius / 2, 0, Math.PI * 2);
	ctx.fill();
	// ctx.drawImage(starCanvas, this.x, this.y, this.radius, this.radius);
}

Star.prototype.update = function() {
	this.x += this.speedX;

	if(this.x < -this.radius || this.x > ww + this.radius) {
		this.x = 0 - this.radius;
	}
}

for(var i = 0; i < starAmount; i++) {
	stars.push(new Star());
}


////////// CANVAS //////////

canvas.width = ww;
canvas.height = wh;
var ctx = canvas.getContext('2d');


////////// RENDER //////////

var render = new FpsCtrl(30, function(e) {

	// stars

	ctx.clearRect(0, 0, ww, wh);

	for(var i = 0; i < stars.length; i++) {
		stars[i].draw();
		stars[i].update();
	}
});

render.start();
