var path = require('path');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	entry: {
		index: './_src/_scripts/index.js',
		privacy: './_src/_scripts/privacy.js',
		credits: './_src/_scripts/credits.js'
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, '_dist/js')
	},
	plugins: [
		// new UglifyJSPlugin()
	]
};
